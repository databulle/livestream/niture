# Niture

A few scripts to discover [Requests](https://requests.readthedocs.io/en/latest/) and [Requests HTML](https://requests.readthedocs.io/projects/requests-html/en/latest/), and start working with HTTP requests and simple scraping for SEO.  

For more details and some context (in French), check out the replays on YouTube:  
- https://www.youtube.com/watch?v=H_Ka_MIs5t8  
- https://www.youtube.com/watch?v=VzJJkTJQTBE  

Join us each wednesday at 5pm CET on <https://www.twitch.tv/diije> for one our of live coding Python for SEO!  


## Setup

1. Optionnal: setup your virtual environment with your prefered tool  
2. Install dependencies by running `pip install -r requirements.txt`  
3. Run the scripts!  