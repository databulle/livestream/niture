import requests
import csv
from validator_collection import validators, errors

def main(urls):
    for url in urls:
        r = requests.get(url["url"])

        # si redirection
        if r.history:
            if not (url["status"] == r.history[0].status_code):
                print(url["url"], "NOT OK", url["status"], r.history[0].status_code)
        # sinon
        elif not (url["status"] == r.status_code):
            print(url["url"], "NOT OK", url["status"], r.status_code)

if __name__ == "__main__":
    
    urls = []
    with open("urls.csv", 'r', newline="") as file:
        reader = csv.DictReader(file, delimiter=",")
        for row in reader:
            try:
                validators.url(row["url"])
            except errors.InvalidURLError as e:
                print(e)
                exit(1)

            urls.append({
                "url": row["url"],
                "status": int(row["status"]),
            })

    main(urls)