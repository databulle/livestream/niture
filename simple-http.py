import requests

def main(urls):
    for url in urls:
        r = requests.get(url)
        
        # redirections
        if r.history:
            for hist in r.history:
                print(hist.url, hist.status_code)
            print(r.url, r.status_code)
        
        # pas de redirection
        else:
            print(url, r.status_code)

if __name__ == '__main__':
    urls = [
        "https://www.databulle.com/",
        "https://httpbin.org/status/418",
        "https://httpbin.org/redirect/3",
    ]

    main(urls)

