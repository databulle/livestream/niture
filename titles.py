import requests
import csv
from validator_collection import validators, errors
import argparse
from requests_html import HTML
from unidecode import unidecode

def main(args):
    urls = []
    with open(args.input, 'r') as file:
        reader = csv.DictReader(file, delimiter=args.sep)
        for row in reader:
            try:
                validators.url(row["url"])
            except errors.InvalidURLError as e:
                print(e)
                exit(1)

            urls.append(row)
        file.close()

    for url in urls:
        r = requests.get(url["url"])

        # si redirection
        if r.history:
            print(f"Error: {url['url']} is redirected.")
            continue
        # sinon
        elif not (200 == r.status_code):
            print(f"Error: response code for {url['url']} is {r.status_code}")
            continue

        html = HTML(html=r.text)

        if len(html.xpath("//title")) > 1:
            print(f"Error: found {len(html.xpath('//title'))} titles on {url['url']}")
            continue
        if len(html.xpath("//title")) == 0:
            print(f"Error: no title on {url['url']}")
            continue

        if not (unidecode(html.xpath("//title/text()")[0]) == unidecode(url["title"])):
            print(f"Error: wrong title on {url['url']}")

        print(f"OK: {url['url']}")

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--input", type=str, 
        help="List of URLs and titles", required=True)
    parser.add_argument("--delay", type=float,
        help="Delay between request", default=1)
    parser.add_argument("--sep", type=str,
        help="Input CSV separator", default=";")

    args = parser.parse_args()

    main(args)

